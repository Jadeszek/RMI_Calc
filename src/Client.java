import java.rmi.*;
import java.util.Scanner;

public class Client {

    public static void main(String[] args){
        try {
            Calculation c = (Calculation) Naming.lookup("calculator");
            Scanner in = new Scanner(System.in);

            double a = in.nextDouble();
            double b = in.nextDouble();

            System.out.println(a + " + " + b + " = " + c.add(a,b));
            System.out.println(a + " - " + b + " = " + c.subtract(a,b));
            System.out.println(a + " * " + b + " = " + c.multiply(a,b));
            System.out.println(a + " / " + b + " = " + c.divide(a,b));
        }
        catch(Exception e){
            System.err.println(e);
        }
    }
}