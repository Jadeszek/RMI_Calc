import java.rmi.*;
import java.rmi.server.*;

public class Calculator extends UnicastRemoteObject implements Calculation {

    public Calculator() throws RemoteException {
    }

    @Override
    public double multiply(double a, double b) throws RemoteException {
        return a * b;
    }

    @Override
    public double add(double a, double b) throws RemoteException {
        return a + b;
    }

    @Override
    public double divide(double a, double b) throws RemoteException {
        return a / b;
    }

    @Override
    public double subtract(double a, double b) throws RemoteException {
        return a - b;
    }

    public static void main(String[] args){
        try {
            Calculator hw = new Calculator();
            Naming.bind("calculator", hw);
        }
        catch(Exception e){
            System.err.println(e);
        }
    }


}