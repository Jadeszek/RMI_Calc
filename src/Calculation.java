import java.rmi.*;

public interface Calculation extends Remote {
    double multiply(double a, double b) throws RemoteException;
    double add(double a, double b) throws RemoteException;
    double divide(double a, double b) throws RemoteException;
    double subtract(double a, double b) throws RemoteException;
}

